/*
 * Copyright (C) 2021 Concesionario Virtual 
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at 
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

//INTERFAZ hecha por Miguel Gamboa Sánchez.

package src.interfaz;

import java.util.*;
import java.io.*;

import src.estructuras.*;

public class Interfaz{
	
	//colores_________________________________________________
	private static final String colorRojo = "\u001B[31m";
	private static final String colorVerde = "\u001B[32m";
	private static final String colorAzul = "\u001B[34m";
	private static final String colorBlanco = "\u001B[37m";
	private static final String colorCian = "\u001B[36m";
	
	private static String negrita = "\033[1m"; 
    	private static String normal = "\033[0m"; 
    	private static String subrayado = "\033[4m"; 

    	private static String rojo = "\u001B[31m";
    	private static String negro = "\u001B[30m";

    	//interfaz por comandos_________________________________
	
	private static void mostrarAyuda(){

        System.out.println(negrita + colorBlanco + "NOMBRE" + normal);
        System.out.println("\t" + subrayado + colorRojo+ "C O N C E S I O N A R I O   V I R T U A L" + normal);
        System.out.println(negrita + colorBlanco+"\nSYNOPSIS" + normal);
        System.out.println("\tjava -jar concesionarioVirtual.jar " + subrayado + "[interfazgrafica | annadir <modelo(string)> <matrícula(string)> <caballos(int)> <numeroKilometros(int)> | mostrar | ayuda]" + normal);
        System.out.println(negrita + colorBlanco+ "\nDESCRIPCIÓN" + normal);
        System.out.println("\tGestiona una garaje virtual. Permite añadir y mostrar coches.");
        System.out.println(negrita +colorBlanco+ "\nEJEMPLOS" + normal);
	System.out.println(colorVerde+"\tEjemplo 1 Abrir interfaz gráfica:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar interfazgrafica\n" + normal);
        System.out.println(colorVerde+"\tEjemplo 2. Añadido de un contacto:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar annadir Ferrari 1829KIO 6000 132100\n" + normal);
        System.out.println(colorVerde+"\tEjemplo 3. Listado de contactos:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar mostrar\n" + normal);
	System.out.println(colorVerde+"\tEjemplo 4. Borrado de contactos:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar borrado\n" + normal);
	  System.out.println(colorVerde+"\tEjemplo 5. Editado de contactos:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar editado\n" + normal);
	System.out.println(colorVerde+"\tEjemplo 6. Generado y actualizado de hoja de cáculo:\n"+normal);
        System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar hojadecalculo\n" + normal);
        System.out.println(colorVerde+"\tEjemplo 7. Muestra esta ayuda:\n"+normal);
       	System.out.println(negrita + "\t\tjava -jar concesionarioVirtual.jar ayuda\n" + normal);
	}

	public static void ejecutar(String[] instruccion){
       
        if (instruccion.length == 0) mostrarAyuda();

	else if (instruccion[0].equalsIgnoreCase("interfazgrafica") && instruccion.length == 1) interfazgrafica();

        else if (instruccion[0].equalsIgnoreCase("annadir") && instruccion.length == 5){
		int instruccion3int =Integer.parseInt(instruccion[3]);
                int instruccion4int =Integer.parseInt(instruccion[4]);
            
		Coche coche = new Coche(instruccion[1], instruccion[2], instruccion3int, instruccion4int);
	    
		Garaje garaje = new Garaje();
		garaje.Garaje();
        }
	
	else if (instruccion[0].equalsIgnoreCase("mostrar") && instruccion.length == 1){
		try {                                                                       
                                File flr = new File("Concesionario.txt");
                                Scanner reader = new Scanner(flr);                                                     
                                while (reader.hasNextLine()) {                  
                                String data = reader.nextLine();                                                       
                                System.out.println(data);                                                              
                                 }                                                                                     
                                 reader.close();                                                                       
                        } catch (FileNotFoundException error) {                                                        
                                 System.out.println("An error occurred: "+error);
                         }
	}

	else if (instruccion[0].equalsIgnoreCase("borrado") && instruccion.length == 1) Garaje.borrado();

	else if (instruccion[0].equalsIgnoreCase("editado") && instruccion.length == 1) Garaje.editado();

        else if (instruccion[0].equalsIgnoreCase("ayuda") && instruccion.length == 1) mostrarAyuda();

	else if (instruccion[0].equalsIgnoreCase("hojadecalculo") && instruccion.length == 1) Garaje.generarHojaDeCalculo();
       
       	else{
            System.out.println(rojo + "Error en la instrucción" + normal); 
            mostrarAyuda();
        }
	}

	//public_____________________________________________
	
	public static void interfazgrafica(){

	//arrays_________________________

	String[] array = new String[2];
        int[] arrayint = new int[2];

	//interfaz visual_________________________________________
	boolean power = true;
	while(power==true){
		System.out.println(colorRojo+"\n____________________________________________\n\n"+colorVerde+"G A R A J E  V I R T U A L:\n\n"+colorAzul+"Escriba"+colorBlanco+" 1"+colorAzul+" para añadir un vehículo.\nEscriba"+colorBlanco+" 2"+colorAzul+" para mostrar todos los vehículos.\nEscriba"+colorBlanco+" 3"+ colorAzul+" para borrar un vehículo.\nEscriba"+colorBlanco+" 4"+ colorAzul+" para editar un vehículo.\nEscribe"+colorBlanco+" 5"+colorAzul+" para actualizar la hoja de cálculo.\nEscribe"+colorBlanco+" 6"+colorAzul+" para salir.\n\nHecho por "+colorBlanco+"Miguel Gamboa Sánchez"+colorAzul+"."+colorRojo+"\n____________________________________________\n"+colorBlanco);
	
	//opciones_____________________________________________________
	
	Scanner sn = new Scanner(System.in);

	String scannerstring = sn.nextLine();
	
	switch (scannerstring){
		
		//modo escritura
		case "1":
			System.out.println(colorVerde+"\nModo escritura: ");

			System.out.println(colorVerde+"\nIntroduce el "+colorRojo+"modelo: \n"+colorCian);
			String modelo= sn.nextLine();
			array[0] = modelo;

			System.out.println(colorVerde+"\nIntroduce la "+colorRojo+"matrícula: \n"+colorCian);
			String matricula = sn.nextLine();
			array[1]=matricula;
			
			System.out.println(colorVerde+"\nIntroduce el número de "+colorRojo+"CV(int): \n"+colorCian);
			String caballos = sn.nextLine();
			int caballosInt =Integer.parseInt(caballos);
			arrayint[0]=caballosInt;

			System.out.println(colorVerde+"\nIntroduce su número de "+colorRojo+"kilómetros(int): \n"+colorCian);
			String distanciaKilometros = sn.nextLine();
			int distaciaKilometrosInt=Integer.parseInt(distanciaKilometros);
			arrayint[1]=distaciaKilometrosInt;

			
			//añadir respuestas a método coche
			Coche coche = new Coche(array[0], array[1], arrayint[0], arrayint[1]);
			//iniciar método garaje	
			Garaje garaje = new Garaje();
			garaje.Garaje();

			break;
	
		//modo impresión
		case "2":
			System.out.println("Modo impresión: "+colorCian);
			try {
			       	File flr = new File("Concesionario.txt");
      				Scanner reader = new Scanner(flr);
      				while (reader.hasNextLine()) {
        			String data = reader.nextLine();
       				System.out.println(data);
				}

     				 reader.close();
    			} catch (FileNotFoundException error) {
     				 System.out.println("An error occurred: "+error);
   			 }
			break;

		//modo borradoo
		case "3":
			Garaje.borrado();
			break;

		case"4":
			Garaje.editado();
			break;

		case "5":
			Garaje.generarHojaDeCalculo();
			break;

		//apagar programa
		case "6":
			power = false;
			break;
	}
	}
	}
}

