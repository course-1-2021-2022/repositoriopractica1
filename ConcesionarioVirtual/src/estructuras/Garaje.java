/*                     
 * Copyright (C) 2021 Concesionario Virtual                            
 *                                                                     
 * Licensed under the Apache License, Version 2.0 (the "License");     
 * you may not use this file except in compliance with the License.    
 * You may obtain a copy of the License at                             
 *                                                                     
 *      http://www.apache.org/licenses/LICENSE-2.0                     
 *                                                                     
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,   
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

//ESTRUCTURA DE GARAJE hecha por Miguel Gamboa Sánchez.

package src.estructuras;

import src.estructuras.Coche;
import src.interfaz.Interfaz;

import java.io.*;
import java.util.*;
import java.lang.*;

public class Garaje{
	public void Garaje(){	

		//configuración archivo sobre el que queremos escribir
		String nombreFichero = "Concesionario.txt";
                File fl = new File(nombreFichero);	

		//comprobar si el archivo esta creado y si lo está no crearlo:		
		try{
			fl.createNewFile();
			System.out.println("\n_ _ _ _ _ _ _ _ _ _  _ _ _ _ _  _ _ _ _\n\nArchivo "+nombreFichero+ " actualizado\n_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");

		}catch(IOException ex){
			System.out.println("\nError: ex \nEl archivo " +nombreFichero+" ya existía");
		}
		
		//escribir datos en txt:	
		try{
			//llamo a los datos de la clase coche con:
			Coche coche = new Coche();
			
			FileWriter flw = new FileWriter(nombreFichero,true);
			flw.write(coche.toString());
			flw.close();

		}catch(IOException exw){
			System.out.println(exw);
		}
	}

	public static void generarHojaDeCalculo(){

		String ArchivoHoja = "HojaCalculo.txt";

		File flh = new File(ArchivoHoja);
		try{

		flh.createNewFile();

		Scanner lector = new Scanner(new File("Concesionario.txt"));
		Scanner lector2 = new Scanner(new File("HojaCalculo.txt"));

		while (lector2.hasNextLine()){
			System.out.println("Restableciendo hoja de cálculo para asegurar el transpaso de datos...");
			String frase = lector2.nextLine();
			System.out.println(frase + "(eliminado)");
			frase.replace(frase, "");
			//Sistema de guardado para poder pasar al siguiente proceso sin nada en la hoja de cáculo
			FileWriter save = new FileWriter(ArchivoHoja);
			save.close();
			System.out.println("Archivo guardado");

		}
		
		 while (lector.hasNextLine()) {

                        String[] datos = new String [4];

			String frase = lector.nextLine();

			System.out.println("\nLeyendo línea...");

                        if (frase.contains("Modelo: ")) {
                               	datos[0] = frase.replace("Modelo: ", "");
				System.out.println("Modelo detectado");

				frase = lector.nextLine();
				if (frase.contains("Matrícula: ")) {
                                	datos[1] = frase.replace("Matrícula: ", "");
					System.out.println("Matrícula detectada");
                        
					frase = lector.nextLine();
					if (frase.contains("CV: ")) {
                                		datos[2] = frase.replace("CV: ", "");
						System.out.println("CV detectado");
                     
						frase = lector.nextLine();
						if (frase.contains("Número de kilómetros: ")){
							datos[3] = frase.replace("Número de kilómetros: ", "");
							System.out.println("Kilometros detectados");
                        				}
					}
				}
			}

			if (datos[0] != null || datos[1] != null || datos[2] != null ||  datos[3] != null){
				FileWriter flh1 = new FileWriter(ArchivoHoja,true);
				String newline = datos[0]+"\t" + datos[1] +"\t"+ datos[2] +"\t"+ datos[3];
				System.out.println(newline);
                        	
				flh1.write(newline+"\n");
				System.out.println("(Línea transformada a hoja de cáculo)");
                        	flh1.close();
			}
		}

		 }catch(IOException ex){
                        System.out.println("\nHa ocurrido un error, revise bien sus datos.");
                }

	System.out.println("\n_ _ _ _ _ _ _ _ _ _  _ _ _ _ _  _ _ _ _\n\nArchivo "+ArchivoHoja+ " actualizado\n_ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _ _");

	}

	public static void borrado(){

		try{
		String ArchivoHoja = "Concesionario.txt";
		
		System.out.println("Escriba el modelo que quiere eliminar");
		Scanner sc = new Scanner(System.in);
		String busqueda = sc.nextLine();

		Scanner lector = new Scanner(new File(ArchivoHoja));
		int line=0;

		StringBuilder savetxt = new StringBuilder();

		while (lector.hasNextLine()) {

			String frase = lector.nextLine();
			savetxt.append(frase+"\n");
		
		}

		String savetxt2 = savetxt.toString();
			
		Scanner sc2 = new Scanner(savetxt2);

		while (sc2.hasNextLine()){
			
			String lineaBuscada = sc2.nextLine();
			if (lineaBuscada.contains(busqueda)) {
				System.out.println(lineaBuscada+ " contiene " + busqueda);
				String newtxt0 = savetxt2.replace(lineaBuscada, "");
				FileWriter fl = new FileWriter(ArchivoHoja);
				
				if (sc2.hasNextLine()){
					lineaBuscada = sc2.nextLine();
					String newtxt1 = newtxt0.replace(lineaBuscada, "");
					System.out.println(lineaBuscada +"(BORRADA)");

					System.out.println("------\n"+savetxt2+"\n----------");
					if (sc2.hasNextLine()){
                                        	lineaBuscada = sc2.nextLine();
                                        	String newtxt2 = newtxt1.replace(lineaBuscada, "");
                                        	System.out.println(lineaBuscada +"(BORRADA)");

                                        	System.out.println("------\n"+savetxt2+"\n----------");
						if (sc2.hasNextLine()){
		                                        lineaBuscada = sc2.nextLine();
                	                        	String newtxt3 = newtxt2.replace(lineaBuscada, "");
                        	               		System.out.println(lineaBuscada +"(BORRADA)");

                                	        	System.out.println("------\n"+savetxt2+"\n----------");

							if (sc2.hasNextLine()){
                                       				lineaBuscada = sc2.nextLine();
                                			        String newtxt4 = newtxt3.replace(lineaBuscada, "");
        			                                System.out.println(lineaBuscada +"(BORRADA)");

								fl.write(newtxt4);
								fl.close();
                                        		}

                                        	}

                                   	}

				}
			}
		}

		}catch(IOException ex){
			
			System.out.println(ex);
		}
	}

	public static void editado(){
	try{
                String ArchivoHoja = "Concesionario.txt";

                System.out.println("\nEscriba el modelo que quiere editar:\n");
                Scanner sc = new Scanner(System.in);
                String busqueda = sc.nextLine();

		System.out.println("\nEscriba el modelo nuevo:\n");
		Scanner sc1 = new Scanner(System.in);
                String modelo = sc1.nextLine();
		
		System.out.println("\nEscriba la matrícula nueva:\n");
		Scanner sc20 = new Scanner(System.in);
                String matricula = sc20.nextLine();

		System.out.println("\nEscribe los CV nuevos:\n");
		Scanner sc3 = new Scanner(System.in);
                String cv = sc3.nextLine();
		
		System.out.println("\nEscribe el número de kilómetros nuevos:\n");
		Scanner sc4 = new Scanner(System.in);
                String km = sc4.nextLine();


                Scanner lector = new Scanner(new File(ArchivoHoja));
                int line=0;

                StringBuilder savetxt = new StringBuilder();

                while (lector.hasNextLine()) {

                        String frase = lector.nextLine();
                        savetxt.append(frase+"\n");

                }

                String savetxt2 = savetxt.toString();

                Scanner sc2 = new Scanner(savetxt2);

                while (sc2.hasNextLine()){

                        String lineaBuscada = sc2.nextLine();
                        if (lineaBuscada.contains(busqueda)) {
                                System.out.println(lineaBuscada+ " contiene " + busqueda);
                                String newtxt0 = savetxt2.replace(lineaBuscada, "Modelo: "+modelo);
                                FileWriter fl = new FileWriter(ArchivoHoja);

                                if (sc2.hasNextLine()){
                                        lineaBuscada = sc2.nextLine();
                                        String newtxt1 = newtxt0.replace(lineaBuscada, "Matrícula: "+matricula);
                                        System.out.println(lineaBuscada +"(BORRADA)");

                                        if (sc2.hasNextLine()){
                                                lineaBuscada = sc2.nextLine();
                                                String newtxt2 = newtxt1.replace(lineaBuscada, "CV: "+cv);
                                                System.out.println(lineaBuscada +"(BORRADA)");

                                                if (sc2.hasNextLine()){
                                                        lineaBuscada = sc2.nextLine();
                                                        String newtxt3 = newtxt2.replace(lineaBuscada, "Número de kilómetros: "+km);
                                                        System.out.println(lineaBuscada +"(BORRADA)");
							
							System.out.println(newtxt3);
							fl.write(newtxt3);
                                                        fl.close();

                                                }

                                        }

                                }
                        }
                }

                }catch(IOException ex){

                        System.out.println(ex);
	}

	}
	}
