/*                     
 * Copyright (C) 2021 Concesionario Virtual                            
 *                                                                     
 * Licensed under the Apache License, Version 2.0 (the "License");     
 * you may not use this file except in compliance with the License.    
 * You may obtain a copy of the License at                             
 *                                                                     
 *      http://www.apache.org/licenses/LICENSE-2.0                     
 *                                                                     
 * Unless required by applicable law or agreed to in writing, software 
 * distributed under the License is distributed on an "AS IS" BASIS,   
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and 
 * limitations under the License.
 */

//ESTRUCTURA DE COCHE hecha por Miguel Gamboa Sánchez.

package src.estructuras;

import src.estructuras.Garaje;

public class Coche{

	static String modelo;
	static String matricula;
	static int caballos;
	static int numeroKilometros;


	//estructura del constructor coche y sus setters
	public Coche(String modelo, String matricula, int caballos, int numeroKilometros){
		this.modelo=modelo;
		this.matricula=matricula;
		this.caballos=caballos;
		this.numeroKilometros=numeroKilometros;
	}


	//para poder pasar datos de clase a clase:
	public Coche(){
		modelo=modelo;
		matricula=matricula;
		caballos=caballos;
		numeroKilometros = numeroKilometros;
	}

	//getters
	public String getModelo(){
		return modelo;
	}
        public String getMatricula(){
                return matricula;
        }
        public int getCaballos(){
                return caballos;
        }
        public int getNumeroKilometros(){
               return numeroKilometros;
        }

	@Override
	public String toString(){
		return "Modelo: "+getModelo() +"\nMatrícula: " + getMatricula() + "\nCV: " + getCaballos() + "\nNúmero de kilómetros: " + getNumeroKilometros()+"\n\n";
	}
}
