# Concesionario Virtual

"Concesionario Virtual" es un programa que permite almacenar datos de coches desde dentro de una archivo .txt además de poder imprimirlos en consola. A mayores dispone de una interfaz gráfica para facilitar la experencia de un usuaria sin conocimientos informáticos.


## Comenzando 

_Estas instrucciones te permitirán obtener una copia del proyecto en funcionamiento en tu máquina local para propósitos de desarrollo y pruebas._

Lea el apartado **Instalación** para conocer como desplegar el proyecto.


### Pre-requisitos

_Para poder ejecutar todo sin riesgo de error debe tener instalado Java en su dispositivo._ 


### Instalación

_En primer lugar se debe clonar el repositorio en el equipo donde desea ejecutarlo._

```
git clone https://bitbucket.org/course-1-2021-2022/repositoriopractica1.git
```
_A continuación debe ejecutar la función final del makefile desde el directorio ConcesionarioVirtual:_

```
$ make concesionario
```


## Funciones básicas

```
NOMBRE
	C O N C E S I O N A R I O   V I R T U A L

SYNOPSIS
	java -jar concesionarioVirtual.jar [interfazgrafica | annadir <modelo(string)> <matrícula(string)> <caballos(int)> <numeroKilometros(int)> | mostrar | ayuda]

DESCRIPCIÓN
	Gestiona una garaje virtual. Permite añadir y mostrar coches.

EJEMPLOS
	Ejemplo 1 Abrir interfaz gráfica:

		java -jar concesionarioVirtual.jar interfazgrafica

	Ejemplo 2. Añadido de un contacto:

		java -jar concesionarioVirtual.jar annadir Ferrari 1829KIO 6000 132100

	Ejemplo 3. Listado de contactos:

		java -jar concesionarioVirtual.jar mostrar

	Ejemplo 4. Borrado de contactos:

		java -jar concesionarioVirtual.jar borrado

	Ejemplo 5. Editado de contactos:

		java -jar concesionarioVirtual.jar editado

	Ejemplo 6. Generado y actualizado de hoja de cáculo:

		java -jar concesionarioVirtual.jar hojadecalculo

	Ejemplo 7. Muestra esta ayuda:

		java -jar concesionarioVirtual.jar ayuda

```

_Finalmente, si todo lo anterior se ha hecho correctamente, la consola imprimirá una lista con las instrucciones básicas del programa. Esta podrá verla en_ **Funciones básicas**


### Analisis de funciones básicas:

- _La primera función (java -jar concesionarioVirtual.jar interfazgrafica) inicializa la interfaz gráfica. Recomendable para usuarios principiantes._

![alt text](https://bitbucket.org/course-1-2021-2022/repositoriopractica1/raw/d072b7e23eaed45cc45602bacaf9687019b2e749/ConcesionarioVirtual/src/interfaz/Interfaz.png)


- _La segunda función (java -jar concesionarioVirtual.jar Modelo Matrícula CV numeroKilometros) sirve para añadir un nuevo coche y sus características a "Concesionario.txt"._

```
java -jar concesionarioVirtual.jar Ferrari 1829KIO 6000 132100
```

- _La tercera función (java -jar concesionaioVirtual.jar mostrar) imprime el archivo txt que almacena los datos_

```
Modelo: Ferrari
Matricula: 1829KIO
CV: 6000
Número de kilometros: 132100_

```
- _La cuarta función (java -jar concesionarioVirtual.jar borrado) sirve para borrar un vehículo entero._

- _La cuarta función (java -jar concesionarioVirtual.jar editado) sirve para editar los datos introducidos de un vehículo._

- _La quinta función (java -jar concesionarioVirtual.jar hojadecalculo) sirve para generar y actualizar la hoja de cálculo. Esta función envía los datos de "Concesionario.txt" a "HojaDeCalculo.txt" en forma de tabla. Al abrir "HojaDeCalculo.txt" en Excel se reorganizaran los datos por celdas adecuadamente.._ 

- _(java -jar concesionarioVirtual.jar ayuda) inicializa la guía mostrada en_ *Funciones básica*.

## Diagrama de clases

![alt text](https://bitbucket.org/course-1-2021-2022/repositoriopractica1/raw/495652223035bc6a20885c566d4f97a074422677/diagrama_de_clases/diagramadeclases.png)


## Despliegue

_Este proyecto a meyores contiene documentación de APIs en HTML(Javadoc) y un diagrama cajas creado en Umbrello._

## Versionado 

Solo usamos BitBucket para el control de versiones [Link del repositorio](https://bitbucket.org/course-1-2021-2022/repositoriopractica1)

## Licencia

[Apache](https://www.apache.org/licenses/LICENSE-2.0)

![Licencia](https://licensebuttons.net/l/by/3.0/88x31.png "Licencia")

## Autor 

**Miguel Gamboa Sánchez** - [Mail](miguel.gamboasanchez@usp.ceu.es) - [GitHub](https://github.com/MiguelG7)
